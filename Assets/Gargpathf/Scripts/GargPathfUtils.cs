﻿#define DEBUG_PATHFINDING

namespace GargPathf
{
	using UnityEngine;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	
	[System.Serializable]
	public struct AgentType
	{
		[RangeAttribute(0.2f, 20f)]
		public float radius;
		[RangeAttribute(0.2f, 20f)]
		public float height;
		[RangeAttribute(0.0f, 10f)]
		public float climbHeight;
		[RangeAttribute(0.0f, 5f)]
		public float stepOffset;
	}
	public class Node
	{
		public float weight;
		public Vector3 position;
		public Node parentNode;
		public bool hasParent;
		public List<Node> neighbors;
		
		public Node(Vector3 p)
		{
			position = p;
			weight = int.MaxValue;
			neighbors = new List<Node>();
		}
		public void resetNode()
		{
			weight = int.MaxValue;
			hasParent = false;
		}
	}
	public enum PathStatus
	{
		OwO,
		Invalid,
		WaitForIt
	}
	public class Path
	{
		public List<Vector3> corners;
		public PathStatus status;
		
		public Path()
		{
			corners = new List<Vector3>();
			status = PathStatus.WaitForIt;
		}
	}
	public static class GargPathfUtils
	{
		//Self explanatory
		public static int maxConcurrentPathfindingOperations = 2;
		static int concurrentPathfindingOperations;
		
		static Node GetNearestNode(Vector3 position, List<Node> nodes, Dictionary<string, List<Node>> nodesDict)
		{
			Node no = null;
			
			List<Node> nod;
			string key = Mathf.RoundToInt(position.x) + " " + Mathf.RoundToInt(position.z);
			if (nodesDict.TryGetValue(key, out nod))
				foreach(Node n in nod)
					if (no == null || (n.position - position).sqrMagnitude < (no.position - position).sqrMagnitude)
						no = n;
			
			//Couldn't find a node with the same XZ coordinates!
			if (no == null)
				foreach(Node n in nodes)
					if (no == null || (n.position - position).sqrMagnitude < (no.position - position).sqrMagnitude)
						no = n;
			
			return no;
		}
		/// <summary>
		/// Queues a pathfinding process and returns a path. Paths with status WaitForIt are either still in queue
		/// or currently being processed. OwO paths have been processed with a successful result, Invalid paths are..
		/// well, invalid.
		/// </summary>
		/// <param name="sourcePosition">Start position</param>
		/// <param name="targetPosition">End position</param>
		/// <param name="a">Agent struct</param>
		/// <param name="layerMask">Layer mask for raycasting operations</param>
		/// <param name="climb">Use climb value as well as step offset?</param>
		/// <param name="useSphereCast">Use SphereCasting instead of Raycasting</param>
		public static Path GetPathTo(Vector3 sourcePosition, Vector3 targetPosition, AgentType a, int layerMask, bool climb = false, bool useSphereCast = false)
		{
			Path p = new Path();
			
			Pathfind(sourcePosition, targetPosition, p, a, layerMask, climb, useSphereCast);
			
			return p;
		}
		static async void Pathfind(Vector3 sourcePosition, Vector3 targetPosition, Path p, AgentType a, int layerMask, bool climb, bool useSphereCast)
		{
			while (concurrentPathfindingOperations >= maxConcurrentPathfindingOperations)
				await Task.Delay(100);
			
			concurrentPathfindingOperations++;
			
			float startingTime = Time.realtimeSinceStartup;
			
			//Calculate the distance between the origin and the destination
			float distanceToTP = 5 + Vector3.Distance(sourcePosition, targetPosition);
			int intDistanceToTP = Mathf.FloorToInt(distanceToTP);
			
			RaycastHit hit = new RaycastHit();
			
			//Do this to prevent the node graph from becoming too large for the CPU and memory to handle
			//This makes it so if the radius is smaller than the distance to the target point divided by 100,
			//the radius is increased to that number
			a.radius = Mathf.Clamp(a.radius, intDistanceToTP * .01f, intDistanceToTP);
			
			//Set up invisible and temporary "nodes"
			var nodesDictionary = new Dictionary<string, List<Node>>();
			int xzStep = Mathf.Clamp(Mathf.CeilToInt(a.radius), 1, intDistanceToTP);
			int yStep = Mathf.Clamp(Mathf.CeilToInt(a.height), 1, intDistanceToTP);
			//These two are used whenever TryGetValue() has to be called on the dictionary
			List<Node> nod;
			List<string> keys = new List<string>();
			for (int x = -intDistanceToTP * 2; x <= intDistanceToTP * 2; x += xzStep)
			{
				for (int z = -intDistanceToTP * 2; z <= intDistanceToTP * 2; z += xzStep)
				{
					if (z % 100 == 0)
						await Task.Delay (10);
					for (int y = -intDistanceToTP * 2; y <= intDistanceToTP * 2; y += yStep)
					{
						Vector3 scOrigin = new Vector3(x, y, z) + sourcePosition;
						
						float yDiff = (sourcePosition.y - targetPosition.y) * 2;
						if (yDiff < 0)
							yDiff = -yDiff;
						
						if (
							//SphereCast using the agent's radius, with a distance just above 1
							(!useSphereCast || Physics.SphereCast(scOrigin, a.radius, -Vector3.up, out hit, yStep + 0.01f, layerMask, QueryTriggerInteraction.Ignore)) &&
							//Raycast to see if there's any floor to step upon
							(useSphereCast || Physics.Raycast(scOrigin, -Vector3.up, out hit, yStep + 0.01f, layerMask, QueryTriggerInteraction.Ignore)) &&
							//Check sphere to see if the agent can even fit in this node
							!Physics.CheckSphere(hit.point + Vector3.up * (a.height - a.radius), a.radius, layerMask, QueryTriggerInteraction.Ignore)
						)
						{
							string key = Mathf.RoundToInt(hit.point.x) + " " + Mathf.RoundToInt(hit.point.z);
							if (nodesDictionary.TryGetValue(key, out nod))
								nod.Add(new Node(hit.point));
							else
							{
								nodesDictionary.Add(key, new List<Node>(new []{new Node(hit.point)}));
								keys.Add(key);
							}
						}
					}
				}
			}
			#if DEBUG_PATHFINDING
			Debug.Log("Created nodes (" + Mathf.RoundToInt(1000 * (Time.realtimeSinceStartup - startingTime)) + " miliseconds)");
			#endif
			
			//Set the neighbors for the nodes. Sadly, both this and the previous enumeration have to be run on the main thread
			List<Node> nodes = new List<Node>();
			for (int i = 0; i < keys.Count; i++)
			{
				if (nodesDictionary.TryGetValue(keys[i], out nod))
				{
					//if (Random.Range(0, 100) == 0)
					if (i % 100 == 0)
						await Task.Delay (10);
					foreach (var curNode in nod)
					{
						for (int x = -1; x <= 1; x++)
						{
							for (int z = -1; z <= 1; z++)
							{
								string key = Mathf.RoundToInt(curNode.position.x + xzStep * x) + " " + Mathf.RoundToInt(curNode.position.z + xzStep * z);
								if (nodesDictionary.TryGetValue(key, out nod))
								{
									foreach (Node n in nod)
									{
										if (n.neighbors.Contains(curNode))
											curNode.neighbors.Add(n);
										else if (n != curNode)
										{
											float yDiff = (n.position.y - curNode.position.y);
											if (
												(climb && yDiff <= 0.01f + a.stepOffset + a.climbHeight && yDiff >= -(0.01f + a.stepOffset + a.climbHeight)) ||
												(yDiff < a.stepOffset && yDiff > -a.stepOffset)
											)
											{
												Vector3 n1Pos = n.position + Vector3.up * (0.1f + a.stepOffset + a.climbHeight);
												Vector3 n2Pos = curNode.position + Vector3.up * (0.1f + a.stepOffset + a.climbHeight);

												if (
													!Physics.Linecast(n1Pos, n2Pos, layerMask, QueryTriggerInteraction.Ignore) &&
													!Physics.Linecast(n2Pos, n1Pos, layerMask, QueryTriggerInteraction.Ignore)
												)
												{
													curNode.neighbors.Add(n);
													#if DEBUG_PATHFINDING
													Debug.DrawLine(curNode.position, n.position, Color.black - Color.black * 0.5f, 5);
													#endif
												}
											}
										}
									}
								}
							}
						}
						if (curNode.neighbors.Count > 0)
							nodes.Add(curNode);
					}
				}
			}
			#if DEBUG_PATHFINDING
			Debug.Log("Defined neighbors (" + Mathf.RoundToInt(1000 * (Time.realtimeSinceStartup - startingTime)) + " miliseconds)");
			#endif
			
			
			
			//Task<Node> task = Task.Run(() => { return DijkstrasAlgo(sourcePosition, targetPosition, nodes, nodesDictionary); });
			//Node node = task.Result;
			
			//Node node = DijkstrasAlgo(sourcePosition, targetPosition, nodes, nodesDictionary);

			Node dummyNode = new Node(Vector3.zero);
			Node[] nd = new Node[]{dummyNode};
			await Task.Run(() => ExecuteAlgorithm(sourcePosition, targetPosition, nodes, nodesDictionary, nd));
			//Task.Run(() => ExecuteAlgorithm(sourcePosition, targetPosition, nodes, nodesDictionary, nd));
			//while (nd[0] == dummyNode) { await Task.Delay(200); }
			Node node = nd[0];
			#if DEBUG_PATHFINDING
			Debug.Log("Ran algorithm (" + Mathf.RoundToInt(1000 * (Time.realtimeSinceStartup - startingTime)) + " miliseconds)");
			#endif
			
			// While there's still previous node, we will continue.
			List<Node> result = new List<Node>();
			while (node != null)
			{
				result.Add(node);
				Node currentNode = node;
				node = currentNode.parentNode;
			}

			// Reverse the list so that it will be from start to end.
			result.Reverse();
			
			foreach (Node n in result)
				p.corners.Add(n.position);
			
			p.status = p.corners.Count <= 1 ? PathStatus.Invalid : PathStatus.OwO;
			
			#if DEBUG_PATHFINDING
			Debug.LogWarning("Pathfinding completed with " + p.status + " path! (" + Mathf.RoundToInt(1000 * (Time.realtimeSinceStartup - startingTime)) + " miliseconds)");
			#endif
			
			concurrentPathfindingOperations--;
		}
		static void ExecuteAlgorithm(Vector3 start, Vector3 endPos, List<Node> nodes, Dictionary<string, List<Node>> nodesDict, Node[] nd)
		{
			nd[0] = DijkstrasAlgo(start, endPos, nodes, nodesDict);
		}
		static Node DijkstrasAlgo(Vector3 start, Vector3 endPos, List<Node> nodes, Dictionary<string, List<Node>> nodesDict)
		{
			// Nodes that are unexplored
			List<Node> unexplored = new List<Node>();

			// We add all the nodes we found into unexplored.
			foreach (Node n in nodes)
			{
				n.resetNode();
				unexplored.Add(n);
			}

			// Set the starting node weight to 0;
			Node startNode = GetNearestNode(start, nodes, nodesDict);
			startNode.weight = 0;
			
			Node end = GetNearestNode(endPos, nodes, nodesDict);
			
			//Debug.DrawLine(startNode.position, end.position, Color.cyan, 90);

			while (unexplored.Count > 0)
			{
				// Sort the explored by their weight in ascending order.
				unexplored.Sort((x, y) => x.weight.CompareTo(y.weight));

				// Get the lowest weight in unexplored.
				Node current = unexplored[0];

				// If we reach the end node, we will stop.
				if (current == end)
					return end;
				
				//Remove the node, since we are exploring it now.
				unexplored.Remove(current);
				
				Node currentNode = current;
				List<Node> neighbours = currentNode.neighbors;
				foreach (Node neighNode in neighbours)
				{
					Node node = neighNode;

					// We want to avoid those that had been explored and is not walkable.
					if (unexplored.Contains(neighNode))
					{
						// Get the distance of the object.
						float distance = (neighNode.position - current.position).sqrMagnitude;
						distance = currentNode.weight + distance;

						// If the added distance is less than the current weight.
						if (distance < node.weight)
						{
							// We update the new distance as weight and update the new path now.
							node.weight = distance;
							node.parentNode = current;
							node.hasParent = true;
						}
					}
				}
			}
			
			return null;
		}
	}
}