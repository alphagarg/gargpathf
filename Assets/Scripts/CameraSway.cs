﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSway : MonoBehaviour
{
	public Transform dude;
    void Update()
    {
    	transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dude.position - transform.position), Time.deltaTime * .03f);
    	transform.eulerAngles += new Vector3(Mathf.PerlinNoise(Time.time, .5f) - .5f, Mathf.Sin(Time.time) - .5f, 0) * Time.deltaTime * .1f;
    }
}
