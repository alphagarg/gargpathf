﻿using UnityEngine;
using GargPathf;

public class TestPathfinder : MonoBehaviour
{
	public Transform targetPosition;
	public AgentType at;
	public Path pth;
	Transform t;
	int currentCorner;
	void Start()
	{
		t = transform;
	}
	void FixedUpdate()
	{
		if (targetPosition)
		{
			if (pth == null || ((pth.status != PathStatus.WaitForIt || pth.status == PathStatus.Invalid) && (pth.corners.Count == 0 || (pth.corners[pth.corners.Count - 1] - targetPosition.position).sqrMagnitude > 1)))
			{
				pth = GargPathfUtils.GetPathTo(t.position, targetPosition.position, at, ~(1 << 8), true, true);
				currentCorner = 0;
			}
			if (pth.status == PathStatus.OwO)
			{
				if (pth.corners.Count > currentCorner)
				{
					t.position = Vector3.MoveTowards(t.position, pth.corners[currentCorner], Time.fixedDeltaTime * 4);
					
					if ((pth.corners[currentCorner] - t.position).sqrMagnitude < 0.01f)
						currentCorner++;
				}
				
				for (int i = 1; i < pth.corners.Count; i++)
					Debug.DrawLine(pth.corners[i - 1], pth.corners[i], Color.red, Time.fixedDeltaTime * 1.5f);
			}
		}
	}
	
	void Update()
	{
		if (Input.GetButtonDown("Fire1") && Camera.main)
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
				targetPosition.position = hit.point;
		}
	}
}
